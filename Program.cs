using api_building_access.Models;
using api_building_access.Services;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
ConfigurationManager configuration = builder.Configuration;

//bind the app setting.json and environmentconfigurations
builder.Services.Configure<BarSettings>(configuration.GetSection("BarSettings"));
builder.Services.AddSingleton<IBarSettings>(d => d.GetRequiredService<IOptions<BarSettings>>().Value);
builder.Services.AddSingleton<BeerService>();//22:08
builder.Services.AddSingleton<BuildingService>();
builder.Services.AddSingleton<SpaceService>();
builder.Services.AddSingleton<SeatService>();
builder.Services.AddSingleton<UserBuildings>();


builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
