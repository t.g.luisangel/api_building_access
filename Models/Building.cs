using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace api_building_access.Models
{

    public class Building
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string? Id { get; set; }
        [BsonElement("name")]
        public string? Name { get; set; }
        [BsonElement("address")]
        public string? Address { get; set; }
        [BsonElement("country")]
        public string? Country { get; set; }
        [BsonElement("status")]
        [BsonDefaultValue(1)]
        public int Status { get; set; }
        [BsonElement("on_site_instructions")]
        public string? On_Site_Instructions { get; set; }
    }
}