using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace api_building_access.Models
{

    public class Space
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string? Id { get; set; }
        [BsonElement("name")]
        public string? Name { get; set; }
        [BsonElement("building_id")]
        public string? BuildingID { get; set; }
        [BsonElement("status")]
        public int Status { get; set; }
        [BsonElement("capacity")]
        public int Capacity { get; set; }
        [BsonElement("mapa_layout")]
        public string? MapaLayout { get; set; }
    }
}