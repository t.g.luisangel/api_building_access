using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace api_building_access.Models
{

    public class Seat
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string? Id { get; set; }
        [BsonElement("name")]
        public string? Name { get; set; }
        [BsonElement("space_id")]
        public string? SpaceID { get; set; }
        [BsonElement("building_id")]
        public string? BuildingID { get; set; }
        [BsonElement("status")]
        [BsonDefaultValue(1)]
        public int Status { get; set; }
        [BsonElement("assigned_to")]
        [BsonDefaultValue("NA")]
        public string? AssignedTo { get; set; }
    }
}