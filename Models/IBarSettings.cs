using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_building_access.Models
{

    public interface IBarSettings
    {
        public string Server { get; set; }
        public string Database { get; set; }
        public string[] Collection { get; set; }
    }
}