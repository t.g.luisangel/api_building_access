using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_building_access.Models
{

    public class BarSettings : IBarSettings
    {
        public string Server { get; set; }
        public string Database { get; set; }
        public string[] Collection { get; set; }
    }
}