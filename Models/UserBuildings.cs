using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace api_building_access.Models
{

    public class UserBuildings
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string? Id { get; set; }
        [BsonElement("building_id")]
        public string? BuildingID { get; set; }
        [BsonElement("user_email")]
        public string? UserEmail { get; set; }
        [BsonElement("status")]
        [BsonDefaultValue(1)]
        public int status {get; set;}

    }
}