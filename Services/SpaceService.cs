using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_building_access.Models;
using MongoDB.Driver;

namespace api_building_access.Services
{

    public class SpaceService
    {
        IMongoCollection<Space> _dbService;
        public SpaceService(IBarSettings settings)
        {
            var cliente = new MongoClient(settings.Server);
            var database = cliente.GetDatabase(settings.Database);
            _dbService = database.GetCollection<Space>(settings.Collection[2]);
        }

        public async Task<List<Space>> Get()
        {
            return await _dbService.Find(_space => true).ToListAsync();
        }
        public async Task<Space> Create(Space space)
        {
            await _dbService.InsertOneAsync(space);
            return space;
        }

        public async Task Update(string id, Space space)
        {
            await _dbService.ReplaceOneAsync(_space => _space.Id == id, space);
        }

        public async Task Delete(string id)
        {
            await _dbService.DeleteOneAsync(_space => _space.Id == id);
        }

    }
}