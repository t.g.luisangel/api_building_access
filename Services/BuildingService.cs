using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_building_access.Models;
using MongoDB.Driver;

namespace api_building_access.Services
{

    public class BuildingService
    {
        private IMongoCollection<Building> _dbService;
        public BuildingService(IBarSettings settings)
        {
            var cliente = new MongoClient(settings.Server);
            var database = cliente.GetDatabase(settings.Database);
            _dbService = database.GetCollection<Building>(settings.Collection[1]);
        }

        public async Task<List<Building>> Get()
        {
            return await _dbService.Find(_building => true).ToListAsync();
        }
        public async Task<Building> Create(Building building)
        {
            await _dbService.InsertOneAsync(building);
            return building;
        }

        public async Task Update(string id, Building building)
        {
            await _dbService.ReplaceOneAsync(_building => _building.Id == id, building);
        }
        public async Task Delete(string id)
        {
            await _dbService.DeleteOneAsync(_building => _building.Id == id);
        }
    }
}