using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_building_access.Models;
using MongoDB.Driver;

namespace api_building_access.Services
{

    public class SeatService
    {
        private IMongoCollection<Seat> _dbService;

        public SeatService(IBarSettings settings)
        {
            var cliente = new MongoClient(settings.Server);
            var database = cliente.GetDatabase(settings.Database);
            _dbService = database.GetCollection<Seat>(settings.Collection[3]);
        }
        public async Task<List<Seat>> Get()
        {
            return await _dbService.Find(_space => true).ToListAsync();
        }

        public async Task<Seat> Create(Seat seat)
        {
            await _dbService.InsertOneAsync(seat);
            return seat;
        }
        public async Task Update(string id, Seat seat)
        {
            await _dbService.ReplaceOneAsync(_seat => _seat.Id == id, seat);
        }
        public async Task Delete(string id)
        {
            await _dbService.DeleteOneAsync(_seat => _seat.Id == id);
        }
    }
}