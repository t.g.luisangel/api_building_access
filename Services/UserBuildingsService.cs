using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using api_building_access.Models;

namespace api_building_access.Services
{

    public class UserBuildingsService
    {
        IMongoCollection<UserBuildings> _dbService;

        public UserBuildingsService(IBarSettings settings)
        {
            var cliente = new MongoClient(settings.Server);
            var database = cliente.GetDatabase(settings.Database);
            _dbService = database.GetCollection<UserBuildings>(settings.Collection[4]);
        }

        public async Task<List<UserBuildings>> Get()
        {
            return await _dbService.Find(_userBuildings => true).ToListAsync();
        }
        public async Task<UserBuildings> Create(UserBuildings userBuildings)
        {
            await _dbService.InsertOneAsync(userBuildings);
            return userBuildings;
        }

        public async Task Update(string id, UserBuildings userBuildings)
        {
            await _dbService.ReplaceOneAsync(_userBuildings => _userBuildings.Id == id, userBuildings);
        }
        public async Task Delete(string id)
        {
            await _dbService.DeleteOneAsync(_userBuildings => _userBuildings.Id == id);
        }
    }
}