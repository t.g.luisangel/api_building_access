using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

namespace api_building_access.Controllers
{


    [Route("[controller]")]
    [ApiController]
    public class PruebaRuta : ControllerBase
    {
        private readonly ILogger<PruebaRuta> _logger;
        public PruebaRuta(ILogger<PruebaRuta> logger)
        {
            _logger = logger;

        }

        [HttpGet(Name = "pruebaruta")]
        public ActionResult Get()
        {
            return Ok();
        }
    }
}