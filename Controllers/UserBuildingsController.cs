using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api_building_access.Models;
using api_building_access.Services;

namespace api_building_access.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserBuildingsController : ControllerBase
    {
        private UserBuildingsService _userBuildingsService;
        public UserBuildingsController(UserBuildingsService userBuildingsService)
        {
            _userBuildingsService = userBuildingsService;
        }
        [HttpGet]
        public async Task<ActionResult<List<UserBuildings>>> Get()
        {
            return await _userBuildingsService.Get();
        }
        [HttpPost]
        public async Task<ActionResult<UserBuildings>> Create(UserBuildings userBuildings)
        {
            await _userBuildingsService.Create(userBuildings);
            return Ok(userBuildings);
        }
        [HttpPut]
        public async Task<ActionResult> Update(UserBuildings userBuildings)
        {
            await _userBuildingsService.Update(userBuildings.Id!, userBuildings);
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            await _userBuildingsService.Delete(id);
            return Ok();
        }
    }
}