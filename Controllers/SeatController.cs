using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api_building_access.Services;
using api_building_access.Models;

namespace api_building_access.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class SeatController : ControllerBase
    {
        private SeatService _seatService;
        public SeatController(SeatService seatService)
        {
            _seatService = seatService;
        }
        [HttpGet]
        public async Task<ActionResult<List<Seat>>> Get()
        {
            return await _seatService.Get();
        }
        [HttpPost]
        public async Task<ActionResult<Seat>> Create(Seat seat)
        {
            await _seatService.Create(seat);
            return Ok(seat);
        }
        [HttpPut]
        public async Task<ActionResult> Update(Seat seat)
        {
            await _seatService.Update(seat.Id!, seat);
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            await _seatService.Delete(id);
            return Ok();
        }
    }
}