using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api_building_access.Services;
using api_building_access.Models;

namespace api_building_access.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpaceController : ControllerBase
    {
        private SpaceService _spaceService;
        public SpaceController(SpaceService spaceService)
        {
            _spaceService = spaceService;
        }
        [HttpGet]
        public async Task<ActionResult<List<Space>>> Get()
        {
            return await _spaceService.Get();
        }
        [HttpPost]
        public async Task<ActionResult<Space>> Create(Space space)
        {
            await _spaceService.Create(space);
            return Ok(space);
        }
        [HttpPut]
        public async Task<ActionResult> Update(Space space)
        {
            await _spaceService.Update(space.Id!, space);
            return Ok(space);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            await _spaceService.Delete(id);
            return Ok();
        }
    }
}