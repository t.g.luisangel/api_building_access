using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using api_building_access.Services;
using api_building_access.Models;

namespace api_building_access.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BeerController : ControllerBase
    {
        private BeerService _beerService;
        public BeerController(BeerService beerService)
        {
            _beerService = beerService;
        }

        [HttpGet]
        public ActionResult<List<Beer>> Get()
        {
            Console.WriteLine("entre al create");
            return _beerService.Get();
        }
        [HttpPost]
        public ActionResult<Beer> Create(Beer beer)
        {
            _beerService.Create(beer);
            Console.WriteLine("entre al create");
            return Ok(beer);
        }
        [HttpPut]
        public ActionResult Update(Beer beer)
        {
            _beerService.Update(beer.Id!, beer);
            return Ok();
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            _beerService.Delete(id);
            return Ok();
        }

    }
}