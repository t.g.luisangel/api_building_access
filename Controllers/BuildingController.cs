using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api_building_access.Services;
using api_building_access.Models;

namespace api_building_access.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BuildingController : ControllerBase
    {
        private readonly ILogger<BuildingController> _logger;
        public BuildingService _buildingService;
        public BuildingController(BuildingService buildingService, ILogger<BuildingController> logger)
        {
            _buildingService = buildingService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<List<Building>>> Get()
        {
            return await _buildingService.Get();
        }
        [HttpPost]
        public async Task<ActionResult<Building>> Create(Building building)
        {
            await _buildingService.Create(building);
            return Ok(building);
        }

        [HttpPut]
        public async Task<ActionResult> Update(Building building)
        {
            await _buildingService.Update(building.Id!, building);
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            await _buildingService.Delete(id);
            return Ok();
        }
    }
}